OLD_LAYOUTS = $(notdir $(basename $(wildcard _layouts/*.html)))

all: install-dependencies
	./.i18n.sh
	jekyll build --verbose --trace

clean:
	rm -rf _site

lint:
	@for X in $(OLD_LAYOUTS); do \
		grep -rl "layout: $$X" . | while read Y; do \
			echo "W: $$Y is using legacy layout '$$X'"; \
		done \
	done
	@grep -rl "href=[\"']/" . | while read X; do \
		echo "W: $$X is using URIs that are not using '{{ \"/foo\" | prepend: site.baseurl }}'"; \
	done

install-dependencies:
	# make sure jekyll is installed, so bundle can use that
	dpkg -l jekyll po4a > /dev/null
	# install jekyll-plugins from their git repos
	bundle install --local

serve:
	bundle exec jekyll serve --watch
