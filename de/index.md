---
layout: no_sidebar
titel: Startseite
title_head: Reproduzierbare Builds &mdash; eine Reihe von Software-Entwicklungspraktiken, die einen unabhängig überprüfbaren Pfad vom Quellcode zum Binärcode schaffen
order: 0
lang: de
permalink: /
---

<div class="text-center mt-md-5 mb-4">
    <a href="{{ "/" | prepend: site.baseurl }}">
        <img class="img-fluid" src="{{ "/assets/images/logo-text.svg" | prepend: site.baseurl }}" alt="Reproduzierbare Builds" style="height: 150px;" />
    </a>

    <p class="lead mt-3 mt-md-5 mx-md-5 px-md-5">
        <strong>Reproduzierbare Builds</strong> sind eine Reihe von Software-Entwicklungen
        Praktiken, die einen unabhängig überprüfbaren Pfad von der Quelle aus schaffen
        in binären&nbsp;Code.

        <small class="d-none d-sm-inline">
            (<a href="{{ "/docs/definition/" | prepend: site.baseurl }}">mehr</a>)
        </small>
    </p>
</div>

{% include nav_buttons.html %}

## Warum ist das wichtig?

Zwar darf jeder den Quellcode freier und quelloffener Software auf
böswillige Schwachstellen untersuchen, die meiste Software wird jedoch vorkompiliert distributiert, ohne Möglichkeit zur
Überprüfung, ob diese übereinstimmen.

Dies bietet Anreize für Angriffe auf Entwickler, die Software veröffentlichen, nicht nur über
traditionelle Ausbeutung, aber auch in Form von politischer Einflussnahme,
Erpressung oder sogar die Androhung von Gewalt.

Dies ist insbesondere ein Anliegen von Entwicklern, die im Bereich des Datenschutzes oder
Sicherheitssoftware: Ein Angriff auf diese führt in der Regel zur Kompromittierung
besonders politisch sensible Ziele wie Dissidenten, Journalisten und
Whistleblowers sowie alle, die sicher unter einer
repressives Regime.

Während einzelne Entwickler ein natürliches Ziel sind, fördert es zusätzlich
Angriffe auf Gebäudeinfrastruktur, da ein erfolgreicher Angriff Zugang zu
eine große Anzahl von nachgeschalteten Computersystemen. Durch Modifikation der generierten
Binärdateien hier, anstatt den Upstream-Quellcode zu modifizieren, unerlaubte Änderungen
sind für ihre ursprünglichen Autoren und Benutzer im Wesentlichen unsichtbar.

Die Motivation hinter dem **Reproduzierbare Builds** Projekt ist daher, Folgendes zu ermöglichen
Verifizierung, dass keine Schwachstellen oder Hintertüren eingeführt wurden während
diesen Kompilierungsprozess. Bei vielversprechenden **identischen Ergebnissen** sind immer
die aus einer bestimmten Quelle generiert wird, erlaubt dies **mehrere Dritte** zu kommen
zu einem Konsens über ein "richtiges" Ergebnis, wobei alle Abweichungen als verdächtig hervorgehoben werden
und einer Prüfung wert.

Diese Fähigkeit, zu bemerken, wenn ein Entwickler kompromittiert wurde, ist
schreckt solche Bedrohungen oder Angriffe, die von vornherein als solche auftreten, ab
Ein Kompromiss würde schnell gefunden werden. Dies bietet den Spitzenkräften Komfort
dass sie nicht nur bedroht werden können, sondern auch nicht dazu gezwungen werden
ihre Kollegen oder Endbenutzer ausnutzen oder bloßstellen.

[Mehrere freie Software-Projekte]({{ "/who/" | vor: site.baseurl }})
bereits jetzt oder demnächst reproduzierbare Builds zur Verfügung stellen.

## How?

First, the **build system needs to be made entirely deterministic**:
transforming a given source must always create the same result. For example,
the current date and time must not be recorded and output always has to be
written in the same order.

Second, the set of tools used to perform the build and more generally the
**build environment** should either be **recorded or pre-defined**.

Third, users should be given a way to recreate a close enough build
environment, perform the build process, and **validate that the output matches
the original build**.

Learn more about [how to make your software build reproducibly…]({{ "/docs" | prepend: site.baseurl }})

## Recent monthly reports

<ul class="list-unstyled">
    {% assign reports = site.reports | sort: 'year, month' | where: 'draft', 'false' | reverse %}
    {% for x in reports limit: 3 %}
    <li>
        <span class="text-muted">{{ x.published | date: "%b %-d, %Y" }}</span>:
        <a href="{{ x.url | prepend: site.baseurl }}">{{ x.title }}</a>
    </li>
    {% endfor %}
</ul>

([See all]({{ "/news/" | prepend: site.baseurl }}).)

## Recent news

<ul class="list-unstyled">
    {% for post in site.posts limit: 3 %}
    <li>
        <span class="text-muted">{{ post.date | date: "%b %-d, %Y" }}</span>:
        <a href="{{ post.url | prepend: site.baseurl }}">{{ post.title }}</a>
    </li>
    {% endfor %}
</ul>

([See all]({{ "/news/" | prepend: site.baseurl }}).)

<br>

{% include nav_buttons.html %}
